package us.mikebartosh.minecraft.anviltooexpensive.mixin;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.AnvilScreen;
import net.minecraft.client.gui.screen.ingame.ForgingScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.StringVisitable;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(AnvilScreen.class)
public abstract class MixinAnvilScreen
        extends ForgingScreen<AnvilScreenHandler> {

    @Shadow @Final private PlayerEntity player;

    public MixinAnvilScreen(AnvilScreenHandler handler, PlayerInventory playerInventory, Text title, Identifier texture) {
        super(handler, playerInventory, title, texture);
    }

    @Inject(method = "drawForeground", at = @At("HEAD"), cancellable = true)
    protected void $anviltooexpensiveOnAnvilScreen(DrawContext context, int mouseX, int mouseY, CallbackInfo ci) {
        super.drawForeground(context, mouseX, mouseY);
        int i = ((AnvilScreenHandler) this.handler).getLevelCost();
        Object text;
        text = Text.translatable("container.repair.cost", new Object[]{i});

        int color = 8453920;
        if (player.experienceLevel < i && !player.getAbilities().creativeMode) {
            color = 16736352;
        }

        // Get block pos
//        int x = (int) player.getBlockPos().getX();
//        int y = (int) player.getBlockPos().getY();
//        int z = (int) player.getBlockPos().getZ();

        // Get world
//        final World world = player.getWorld();
//
//        if (i == 69) {
//            color = 106050159;
//            //world.playSound(x, y, z, SoundEvents.ENTITY_PLAYER_LEVELUP, SoundCategory.BLOCKS, 1.0F, 1.0F, false);
//            final Identifier TEXTURE = new Identifier("textures/gui/container/furnace.gif");
//            int b_x = this.backgroundWidth / 2;
//            int b_y = this.backgroundHeight / 2;
//            context.drawTexture(TEXTURE, b_x, b_y, 100, 100, 100, 100);
//            context.fillGradient(b_x, b_y, b_x + 100, b_y + 100, 0x000000, 0x45900);
//        }

        if (text != null) {
            int k = this.backgroundWidth - 8 - this.textRenderer.getWidth((StringVisitable) text) - 2;
            context.fill(k - 2, 67, this.backgroundWidth - 8, 79, 1325400064);
            context.drawTextWithShadow(this.textRenderer, (Text) text, k, 69, color);
        }
        ci.cancel();
    }
}
